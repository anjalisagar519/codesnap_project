import { useState } from 'react'
import './App.css'

function App() {

  return (
    <div className='relative'>
      <header className='flex felx-row justify-center items-center px-12 h-20 shadow-lg '>
       <div className='container flex flex-row justify-between items-center'>
       <span className='text-lg font-bold'>CodeSnap</span>
        <nav>
          <ul className='flex fex-row gap-6'>
            <li>
              <a href='#'>Home</a>
            </li>
            <li>
              <a href='#'>About</a>
            </li>
            <li>
              <a href='#'>Articles</a>
            </li>
            <li>
              <a href='#'>Contact</a>
            </li>

          </ul>
        </nav>
        <div>
          <a className='bg-violet-600 py-2 px-4 text-white font-bold rounded-sm hover:bg-violet-500' href='#'>Log In</a>
        </div>
       </div>
      </header>
      <main className='py-14'>
        <section className='container mx-auto px-12 grid grid-cols-2 gap-12 items-center'>
          <div className='max-w-xl'>
          <h1 className='text-5xl font-semibold mb-8'>start coding with smile</h1>
          <p>"Embark on a joyful coding journey with StartCodingWithSmile – where learning meets laughter, making every code a step closer to success!"</p>
          <div className='mt-8 flex felx-row gap-4'>
          <a className='inline-block bg-violet-600 py-2 px-4 text-white font-bold rounded-sm hover:bg-violet-500' href='#'>Explore contents</a>
          <a className='inline-block border border-violet-600 py-2 px-4 text-violet-600 font-bold rounded-sm hover:bg-violet-100' href='#'>Contact us</a>
        </div>
          </div>
          <img className='w-full object-cover object-center rounded-lg' src='/images/hero_image.jpg' alt='showing a programer image'></img>
       </section>
       
      </main>
      <footer></footer>
    </div>
  )
}

export default App
